import $ from 'jquery'

const changeValue = {

  init: function () {

    const $number = $('.js-value-number'),
          $increaseInNumber = $('.js-increase-number'),
          $decreaseInNumber = $('.js-decrease-number')

    $decreaseInNumber.on('click', function() {

      if (parseInt($number.val()) === 0)
        return

      const newNumber = parseInt($number.val() - 1)
      $number.val(newNumber)
    })

    $increaseInNumber.on('click', function() {

      const newNumber = parseInt($number.val()) + 1
      $number.val(newNumber)
    })
  }
}
changeValue.init()
