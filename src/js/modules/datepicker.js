import $ from 'jquery'
import datepickerFactory from 'jquery-datepicker'

datepickerFactory($)


const datapicker = {
  init: function () {
    $( '#datepicker-1' ).datepicker({
        dateFormat: 'dd/mm/yy',
        showAnim: 'slideDown',
        orientation: 'bottom auto'
    })

    $( '#datepicker-2' ).datepicker({
        dateFormat: 'dd/mm/yy',
        showAnim: 'slideDown',
        orientation: 'bottom auto'
    })
  }
}
datapicker.init()
