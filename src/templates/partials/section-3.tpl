<div class="c-content c-content--flights">
    <div class="c-content__desc">
        <div class="c-content__desc__title">
            <h2 class="t-heading-2">Find the cheapest flights</h2>
        </div>
        <div class="c-content__desc__text">
            <p class="t-text-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incid&shy;idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerci&shy;tation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        </div>
    </div>
    <div class="c-content__icon c-content__icon--flights">
        <ul class="c-list c-icons-list">
            <li class="c-list__item">
                <img class="c-list__icon" src="./static/img/umbrella.png" alt="icon umbrella" title="Umbrella">
            </li>
            <li class="c-list__item">
                <img class="c-list__icon" src="./static/img/ticket.png" alt="icon ticket" title="Ticket">
            </li>
            <li class="c-list__item">
                <img class="c-list__icon" src="./static/img/cash.png" alt="icon cash" title="Cash">
            </li>
        </ul>
    </div>
</div>
