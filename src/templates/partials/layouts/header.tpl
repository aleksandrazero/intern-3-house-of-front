{% macro render(_active_no) %}
    {% if _active_no == '' %}
        {% set logo_href='#' %}
    {% else %}
        {% set logo_href='index.html' %}
    {% endif %}
		<header class="l-header u-gutter-sm-top u-gutter-sm-bottom ">
            <div class="c-header">
                <div class="c-header__logo">
                    <img src="./static/img/logo_flywithus.png" alt="logo" title="Logo Fly with us">
                </div>
                <div class="c-header__menu">
                    <ul class="c-list c-list--menu">
                        <li class="c-list__item">
                            <a class="t-text-1" href="#">Flights</a>
                        </li>
                        <li class="c-list__item">
                            <a class="t-text-1" href="#">Hotels</a>
                        </li>
                        <li class="c-list__item">
                            <a class="t-text-1" href="#">Guides</a>
                        </li>
                        <li class="c-list__item">
                            <a class="t-text-1" href="#">Popular</a>
                        </li>
                    </ul>
                </div>
                <div class="c-header__search">
                    <form class="f-form f-form--search">
                        <input class="f-form__input t-text-2 t-upper" type="search" value="search">
                        <div class="f-form__icon">
                            <svg class="o-icon o-icon--search">
                                <use xlink:href="./static/symbol/svg/sprite.symbol.svg#search"></use>
                            </svg>
                        </div>
                    </form>
                </div>
            </div>
        </header>
{% endmacro %}
