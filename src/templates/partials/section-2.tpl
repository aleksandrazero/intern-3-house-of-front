<div class="c-checkbox">
    <form class="f-form f-form--filter">
        <div class="f-form__choice">
            <div class="f-form__city">
                <input class="f-form__input t-text-3" type="text" name="city" value="Current City">
            </div>
            <div class="f-form__destination">
                <input class="f-form__input t-text-3" type="text" name="destination" value="Destination">
            </div>
            <div class="f-form__date f-form__date--start">
                <input id="datepicker-1" class="f-form__input t-text-3" name="start_data" value="23/01/2017">
            </div>
            <div class="f-form__date f-form__date--finish">
                <input id="datepicker-2" class="f-form__input t-text-3" name="finish_data" value="23/01/2017">
            </div>
            <div class="f-form__people">
                <div class="f-form__counter">
                    <span><img class="f-form__img js-increase-number" src="./static/img/arrow.png" alt="arrow" title="Arrow up"></span>
                    <input class="f-form__input js-value-number t-text-4" type="text" name="number" value="2">
                    <span><img class="f-form__img js-decrease-number" src="./static/img/arrow-down.png" alt="arrow" title="Arrow down"></span>
                </div>
            </div>
        </div>
        <div class="f-form__enter">
            <button class="c-button" type="submit"><img class="f-form__img" src="./static/img/go.png" alt="arrow" title="Go"></button>
        </div>
    </form>
</div>

