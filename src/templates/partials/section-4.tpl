<div class="c-content c-content--destination">
    <div class="c-content__desc c-content__desc--destinations">
        <div class="c-content__desc__title">
            <h2 class="t-heading-2">Popular Destinations</h2>
        </div>
        <div class="c-content__desc__text">
            <p class="t-text-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incid&shy;idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerci&shy;tation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Extepteur</p>
        </div>
    </div>
    <div class="c-content__icon">
        <ul class="c-city-list">
            <li class="c-city-list__item">
                <figure class="c-city-list__media">
                    <img class="c-city-list__icon" src="./static/img/photo.png" alt="photo" title="View">
                </figure>
                <div class="c-city-list__desc">
                    <h4 class="t-heading-3 t-upper">New York</h4>
                </div>
            </li>
            <li class="c-city-list__item">
                <figure class="c-city-list__media">
                    <img class="c-city-list__icon" src="./static/img/photo.png" alt="photo" title="View">
                </figure>
                <div class="c-city-list__desc">
                    <h4 class="t-heading-3 t-upper">Dubai</h4>
                </div>
            </li>
            <li class="c-city-list__item">
                <figure class="c-city-list__media">
                    <img class="c-city-list__icon" src="./static/img/photo.png" alt="photo" title="View">
                </figure>
                <div class="c-city-list__desc">
                    <h4 class="t-heading-3 t-upper">Paris</h4>
                </div>
            </li>
            <li class="c-city-list__item">
                <figure class="c-city-list__media">
                    <img class="c-city-list__icon" src="./static/img/photo.png" alt="photo" title="View">
                </figure>
                <div class="c-city-list__desc">
                    <h4 class="t-heading-3 t-upper">London</h4>
                </div>
            </li>
            <li class="c-city-list__item">
                <figure class="c-city-list__media">
                    <img class="c-city-list__icon" src="./static/img/photo.png" alt="photo" title="View">
                </figure>
                <div class="c-city-list__desc">
                    <h4 class="t-heading-3 t-upper">Amsterdam</h4>
                </div>
            </li>
            <li class="c-city-list__item">
                <figure class="c-city-list__media">
                    <img class="c-city-list__icon" src="./static/img/photo.png" alt="photo" title="View">
                </figure>
                <div class="c-city-list__desc">
                    <h4 class="t-heading-3 t-upper">Rio de Janeiro</h4>
                </div>
            </li>
        </ul>
    </div>
    <div class="c-content__button">
        <button class="c-button c-button--destinations t-text-6 t-upper">View more</button>
    </div>
</div>
