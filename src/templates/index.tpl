<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>{% block title %} {{ title }} {% endblock %}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        {% include "head/head-links.tpl" %}
    </head>
    <body>
        {% import "partials/layouts/header.tpl" as l_header %}
        {{ l_header.render() }}
        <section class="l-sec l-sec--s-1">
            <div class="l-inner">
                {% include "partials/section-1.tpl" %}
            </div>
        </section>
        <section class="l-sec l-sec--s-2">
            <div class="l-inner">
                {% include "partials/section-2.tpl" %}
            </div>
        </section>
        <section class="l-sec l-sec--s-3">
            <div class="l-inner u-gutter-lg-top u-gutter-lg-bottom">
                {% include "partials/section-3.tpl" %}
            </div>
        </section>
        <section class="l-sec l-sec--space-img">
            <div class="l-inner">
            </div>
        </section>
        <section class="l-sec l-sec--s-4">
            <div class="l-inner u-gutter-md-top u-gutter-md-bottom">
                {% include "partials/section-4.tpl" %}
            </div>
        </section>
        <section class="l-sec l-sec--space-img">
            <div class="l-inner">
            </div>
        </section>
        <footer class="l-footer" data-css-spacing="top2 tablet(top1) left5 tablet(left3)">
            <div class="l-inner">
            </div>
        </footer>
        <script src="./static/js/app.js"></script>
    </body>
</html>
